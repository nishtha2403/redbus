import React from 'react';
import './Benefits.css';
import SafetyImg from '../../assets/safety.svg';
import CustomerCareImg from '../../assets/customer_care.png';
import FareImg from '../../assets/lowest_Fare.png';
import BenefitsImg from '../../assets/benefits.png';

function Benefits() {
  return (
    <div className="Benefits">
      <h2 className="text-center m-4">WHY BOOK WITH REDBUS</h2>
      <div className="Benefits-cards">
        <div className="card-body">
          <img className="my-3" src={SafetyImg} alt={SafetyImg} />
          <div className="m-5">SAFETY +</div>
          <p className="mx-5">
            With Safety+ we have brought in a set of measures like Sanitized
            buses, mandatory masks etc. to ensure you travel safely.
          </p>
        </div>
        <div className="card-body">
          <img className="my-3" src={CustomerCareImg} alt={CustomerCareImg} />
          <div className="my-5">SUPERIOR CUSTOMER SERVICE</div>
          <p className="mx-5">
            We put our experience and relationships to good use and are
            available to solve your travel issues.
          </p>
        </div>

        <div className="card-body">
          <img className="my-3" src={FareImg} alt={FareImg} />
          <div className="my-5">LOWEST PRICES</div>
          <p className="mx-5">
            We always give you the lowest price with the best partner offers.
          </p>
        </div>
        <div className="card-body">
          <img className="my-3" src={BenefitsImg} alt={BenefitsImg} />
          <div className="my-5">UNMATCHED BENEFITS</div>
          <p className="mx-5">
            We take care of your travel beyond ticketing by providing you with
            innovative and unique benefits
          </p>
        </div>
      </div>
    </div>
  );
}

export default Benefits;

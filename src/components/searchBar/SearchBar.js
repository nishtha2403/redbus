import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import './SearchBar.css';

export class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sourcecity: '',
      destinationcity: '',
      date: '',
      redirect: null,
      searchedData: {}
    };
  }

  handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value });
  };

  handleSumbit = event => {
    event.preventDefault();
    if (
      this.state.sourcecity &&
      this.state.destinationcity &&
      this.state.date
    ) {
      let busDetails = {
        from: this.state.sourcecity,
        to: this.state.destinationcity,
        date: this.state.date,
      };
      this.setState({
        searchedData: busDetails
      }, () => {
        this.setState({redirect: "/bus-tickets"});
      });
    }
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to={{
        pathname: this.state.redirect,
        state: { searchedData: this.state.searchedData }
      }} />;
    }
    return (
      <div className="SearchBar">
        <h1>Online Bus Ticket Booking</h1>
        <h2>Search for Bus Tickets Online</h2>
        <div className="SearchBar-content">
          <form onSubmit={this.handleSumbit}>
            <div className="form-group">
              <input
                type="text"
                placeholder="FROM"
                name="sourcecity"
                list="sourcecity"
                className="form-control"
                onChange={this.handleChange}
              />
              <datalist id="sourcecity">
                <option>Delhi</option>
                <option>Mumbai</option>
                <option>Bangalore</option>
                <option>Pune</option>
                <option>Abohar</option>
                <option>Agra</option>
                <option>Adilabad</option>
              </datalist>
            </div>
            <div className="form-group">
              <input
                type="text"
                placeholder="TO"
                name="destinationcity"
                list="destinationcity"
                className="form-control"
                onChange={this.handleChange}
              />
              <datalist id="destinationcity">
                <option>Delhi</option>
                <option>Mumbai</option>
                <option>Bangalore</option>
                <option>Pune</option>
                <option>Abohar</option>
                <option>Agra</option>
                <option>Adilabad</option>
              </datalist>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="date">
                ON
              </label>
              <input
                type="date"
                className="form-control"
                id="date"
                name="date"
                onChange={this.handleChange}
              />
            </div>
            <button className="btn btn-outline-secondary" type="submit">
                Search Buses
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default SearchBar;

import React, { Component } from 'react';
import { FaRegEye, FaRegEyeSlash} from 'react-icons/fa';
import axios from 'axios';
import './Login.css';
import loginPageLogo from '../../assets/logo_red.png';
import Footer from '../Footer/Footer';
import { Redirect } from 'react-router-dom';

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            emailError: '',
            passwordError: '',
            showPassword: false,
            isLoading: false,
            redirect: null
        };
    }

    handleEmailChange = (event) => {
        let email = event.target.value.trim();
        let emailValidator=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //eslint-disable-line

        const stateToUpdate = {
            email,
            emailError: "Email is required",
        };
        
        stateToUpdate["emailError"] = email.match(emailValidator) ? "" : "Enter a valid email";

        this.setState(stateToUpdate);
    }

    handlePasswordChange = (event) => {
        let password = event.target.value;

        const stateToUpdate = {
            password,
            passwordError: password === '' ? 'Password is required': "",
        }

        if(this.state.email === "") {
            stateToUpdate["emailError"] = 'Email is required';
        }

        this.setState(stateToUpdate);
    }

    handleLoginData = (loginData) => {
        localStorage.clear();

        this.setState({
            isLoading: true
        }, async () => {
            try {
                let response = await axios.post('https://redbus-apis.herokuapp.com/login', loginData);
        
                let responseData = await response.data;
          
                localStorage.setItem("accessToken",responseData.accessToken);
                localStorage.setItem("refreshToken",responseData.refreshToken);
                
                this.setState({
                    isLoading: false
                }, () => {
                    this.props.getUsername(loginData.email);
                    this.setState({
                        redirect: '/'
                    })
                });

            } catch(err) {
                this.setState({
                    isLoading: false,
                    passwordError: err.response && err.response.data && (err.response.data.msg === "Password Incorrect") ? err.response.data.msg : "",
                    emailError: err.response && err.response.data && (err.response.data.msg === "User doesn't exist") ? err.response.data.msg : ""
                });
            } 
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let isFormDirty = false;

        let email = this.state.email;
        let password = this.state.password;
       
        let emailError = this.state.emailError;
        let passwordError = this.state.passwordError;

        if(email === '' && password === '' &&  emailError === '' && passwordError === ''){
            isFormDirty = true;
        } 

        if(isFormDirty) {
            this.setState({
                emailError: 'Email is required',
                passwordError: 'Password is required',
            });
        } else {
            let loginData = {
                email: this.state.email.trim(),
                password: this.state.password
            }
            this.handleLoginData(loginData);
        }
    }

    handleShowPassword = () => {
        this.setState((prevState) => {
            return {
                showPassword: !prevState.showPassword,
            }
        });
    }

    render() {
        if(this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        return (
            <>
                <div className="LoginFormWrapper">
                <form onSubmit={this.handleSubmit} className="Loginform">
                <div className="HeadingImgWrapper">
                    <h1 className="Heading">Sign in</h1>
                    <img src={loginPageLogo} className="ImgStyle" alt=""/>
                </div>

                <div className="form-group mb-3">
                    <label>Email</label>
                    <input className="form-control"
                        type="text" 
                        value={this.state.email} 
                        onChange={this.handleEmailChange}
                        style={this.state.emailError !== '' ? ErrorStyle : (this.state.email !== '' ? SuccessStyle : null )}
                        placeholder="example@domain.com"/>
                    <small className="form-text text-danger">{this.state.emailError}</small>
                </div>

                <div className="form-group mb-3">
                    <label>Password</label>
                    <div className="input-group">
                        <input className="form-control"
                            type={this.state.showPassword ? "text" : "password"} 
                            value={this.state.password} 
                            onChange={this.handlePasswordChange}
                            style={this.state.passwordError !== '' ? ErrorStyle : (this.state.password !== '' ? SuccessStyle : null )}
                            placeholder="password"/>
                        <div className="input-group-append">
                            <button className="btn btn-outline-secondary" type="button"  onClick={this.handleShowPassword}>
                                {this.state.showPassword ? <FaRegEyeSlash /> :  <FaRegEye />  }
                            </button>
                        </div>
                    </div>
                    <small className="form-text text-danger">{this.state.passwordError}</small>
                </div>

                <div className="ButtonSpinnerWrapper">
                    <button className="btn ButtonStyle" type="submit">Submit</button>
                    {
                        this.state.isLoading &&
                        <div className="spinner-border SpinnerStyle" role="status"></div>
                    }
                </div>
            </form>
        </div>
        <Footer />
        </>
        )
    }
}

const ErrorStyle = {
    border: "1px solid red"
}

const SuccessStyle = {
    border: "1.5px solid lightgreen"
}

export default Login

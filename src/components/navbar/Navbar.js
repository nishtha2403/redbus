import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import redBuslogo from '../../assets/logo.png';
import './Navbar.css';
import userIcon from '../../assets/userIcon.png';

export class Navbar extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark NavbarStyle">
          <div className="container-fluid">
            <Link className="navbar-brand LinkStyle" to="/">
              <img
                src={redBuslogo}
                className="d-inline-block align-top"
                alt=""
              />
            </Link>

            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink
                    activeClassName="selected"
                    className="nav-link LinkStyle "
                    to="/login"
                    activeStyle={{
                      fontWeight: 'bold',
                      color: 'white',
                    }}
                  >
                    Sign In
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    activeClassName="selected"
                    className="nav-link LinkStyle"
                    to="/signup"
                    activeStyle={{
                      fontWeight: 'bold',
                      color: 'white',
                    }}
                  >
                    Sign Up
                  </NavLink>
                </li>
              </ul>
              <div className="UserProfileStyle">
                  <p>{this.props.username}</p>
                  <img src={userIcon} alt="" className="UserIconStyle" />
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;

import React, { Component } from 'react';
import axios from 'axios';
import BusDetails from './busDetails/BusDetails';
import SearchData from './searchDetails/SearchData';
import no_bus from '../../assets/no_bus.png';
import './BusTicketsPage.css';

export class BusTicketsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            availableBuses: []
        }
    }

    componentDidMount = () => {
        this.handleSearchData(this.props.location.state.searchedData);
    }

    handleSearchData = async (searchedData) => {
        this.setState({
          isLoading: true,
        });
        try {
          let response = await axios.post(
            'https://redbus-apis.herokuapp.com/busdetails',
            searchedData
          );
          let responseData = await response.data;
          this.setState({
            isLoading: false,
          });
    
          let availableBuses = responseData.availableBuses;
    
          this.setState({
            availableBuses
          });          
        } catch (err) {
          this.setState({
            isLoading: false
          });
          if (err.response) {
            console.log(err.response.data.msg);
          }
          console.log(err);
        }
    };
    
    render() {
        return (
            <div>
                {
                    <SearchData searchedData={this.props.location.state.searchedData} />
                }
                {
                    this.state.isLoading && (
                      <div className="SpinnerWrapper">
                          <div className="spinner-border BusTicketSpinner" role="status"></div>
                      </div>
                    )
                }
                {
                    this.state.errorstatus && (
                        <div
                          style={{
                            fontSize: '1rem',
                            color: 'red',
                            backgroundColor: 'white',
                            textAlign: 'center',
                            width: 'fit-content',
                            fontWeight: '700',
                            margin: '1em auto',
                            padding: '0.1em 1em',
                            borderRadius: '0.7em',
                            opacity: '0.5',
                          }}
                        >
                          {this.state.errorMsg}
                        </div>
                      )
                }
                {
                    !this.state.isLoading && this.state.availableBuses.length > 0 &&
                    this.state.availableBuses.map((bus) => (
                        <BusDetails key={bus.id} busData={bus}/>
                    ))
                }
                {
                    !this.state.isLoading && this.state.availableBuses.length === 0 &&
                    <div className="NoBusFound">
                        <img src={no_bus} alt=""/>
                        <p>Oops! No buses found.</p>
                    </div>
                }
            </div>
        )
    }
}

export default BusTicketsPage

import React, { Component } from 'react';
import axios from 'axios';
import PaymentDetails from '../payment/PaymentDetails';
import Seats from '../seats/Seats';
import './BusDetails.css';

export class BusDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            seatsFetching: false,
            seatData: [],
            showSeats: false,
            selectedSeats: [],
            selectedSeatsObject: [],
            noSeatImage: ''
        }
    }

    fetchSeatDetails = (busIdObj) => {

        this.setState({
            seatsFetching: true
        }, async () => {
            try {
                let response = await axios.post('https://redbus-apis.herokuapp.com/seatdetails', busIdObj);
        
                let responseData = await response.data;

                let seatData = responseData.availableSeats;
    
                this.setState({
                    seatData,
                    seatsFetching: false,
                    showSeats: true
                });

            } catch(err) {
                this.setState({seatsFetching: false} , () => {
                    if(err.response) {
                        this.setState({
                          noSeatImage: 'No seat image available for this bus'
                        })
                       console.log(err.response.data.msg);
                    }  else {
                        console.log("Error Occured! Try after sometime");
                    }
                });
            } 
        });
    }

    handleViewTicket = () => {
        this.setState({
            seatsFetching: true
        }, () => {
            let { id } = this.props.busData;
            let busIdObj = {
                busId: id
            }
            this.fetchSeatDetails(busIdObj);
        })
    }

    handleHideTicket = () => {
        this.setState({
            showSeats: false,
            selectedSeats: []
        })
    }

    getSelectedSeat = (selectedSeats,selectedSeatsObject) => {
        this.setState({
            selectedSeats,
            selectedSeatsObject
        })
    }

    render() {
        console.log(this.props.busData);
        let {id, bus_name, departure, arrival, fare, available_seat, rating, boarding_point, dropping_point } = this.props.busData;
        console.log(boarding_point,dropping_point);
        return (
           <div className="BusDetailSection">
                <div className="BusDetailWrapper">
                    <div className="card BusDetailsStyle">
                        <div className="card-body BusBody">
                            <div className="BusBodySection1">
                                <p className="BusHead">{bus_name}</p>
                                <div className="ElementSection1">
                                    <p className="ElementHead">Departure</p>
                                    <p className="BoldElement">{departure}</p>
                                </div>
                                <div className="ElementSection1">
                                    <p className="ElementHead">Arrival</p>
                                    <p className="ElementBody">{arrival}</p>
                                </div>
                                <div className="ElementSection1">
                                    <p className="ElementHead">Ratings</p>
                                    <p className="ElementBody">{rating}</p>
                                </div>
                                <div className="ElementSection1">
                                    <p className="ElementHead">Fare</p>
                                    <div className="FareWrapper">
                                        <span className="InrStyle">INR</span>
                                        <p className="BoldElement">{fare}</p>
                                    </div>
                                </div>
                                <div className="ElementSection1">
                                    <p className="ElementHead">Seats Available</p>
                                    <p className="SeatsAvailable">{available_seat} Seats available</p>
                                </div>
                            </div>
                            <div className="ButtonSpinnerWrapper">
                                {
                                    this.state.seatsFetching &&
                                    <div className="spinner-border SpinnerStyle" role="status"></div>
                                }
                                <button
                                className={
                                    !this.state.showSeats ? "ViewTicketButton" : "HideTicketButton"
                                }
                                onClick={
                                    !this.state.showSeats ? this.handleViewTicket : this.handleHideTicket }>
                                    {
                                        !this.state.showSeats ? "VIEW SEATS" : "HIDE SEATS"
                                    }
                                </button>
                            </div>
                        </div>
                    </div>
                {
                    this.state.showSeats && 
                    <div className="SeatDetailsSection">
                        <div className="SeatSelection">
                            <div className="SeatInfoStyle">
                                <div className="VacantSeatLegend"></div>
                                <p className="LegendLabel">Available</p>
                                <div className="BookedSeatLegend"></div>
                                <p className="LegendLabel">Unavailable</p>
                            </div>
                            <Seats seatData={this.state.seatData} getSelectedSeat={this.getSelectedSeat}/>
                        </div>
                        {
                            this.state.selectedSeats.length > 0 && 
                            <div className="PaymentSection">
                                <PaymentDetails 
                                selectedSeats={this.state.selectedSeats} 
                                selectedSeatsObject={this.state.selectedSeatsObject}
                                fare={fare} 
                                busId={id}
                                boardingPoints={boarding_point}
                                droppingPoints={dropping_point}
                                departure={departure}
                                arrival={arrival}/>
                            </div>
                        }
                    </div>
                }
            </div>
           </div>
        )
    }
}

export default BusDetails

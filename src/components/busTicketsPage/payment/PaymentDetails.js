import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import './PaymentDetails.css';

export class PaymentDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isUserSelectingBD: true,
            displayBoarding: true,
            displayDropping: false,
            selectedBoarding: '',
            selectedDropping: '',
            redirect: null            
        }
    }
    boardingClicked = () => {
        this.setState({
            displayBoarding: true,
            displayDropping: false
        })
    }
    droppingClicked = () => {
        this.setState({
            displayBoarding: false,
            displayDropping: true
        })
    }
    handleBoarding = (event) => {
        this.setState({
            selectedBoarding: event.target.value,
            displayBoarding: false,
            displayDropping: true
        })
    }
    handleDropping = (event) => {
        this.setState({
            selectedDropping: event.target.value
        })
    }
    continueClicked = () => {
        this.setState( (prevState) => {
            return {
                isUserSelectingBD: !prevState.isUserSelectingBD
            }
        })
    }
    handleProceedToBook = () => {
        this.setState({redirect: "/passenger-details"});
    }
    render() {
        let selectedSeats = this.props.selectedSeats;
        let totalAmount = selectedSeats.length * this.props.fare;
        if (this.state.redirect) {
            let selectedSeatsArrayObject = this.props.selectedSeatsObject;
            let seatPaymentInfo = {
                busId: this.props.busId,
                boardPoint: this.state.selectedBoarding,
                droppingPoint: this.state.selectedDropping,
                amount: totalAmount,
                seatInfo: selectedSeatsArrayObject
            }
            console.log(seatPaymentInfo);
            return <Redirect to={{
              pathname: this.state.redirect,
              state: { seatPaymentInfo }
            }} />;
        }
        return (
            <div>
                <div className="card PaymentCardStyle">
                    <div className="card-body">
                        {
                            this.state.isUserSelectingBD &&
                            <div>
                                <div className="BDHeading">
                                    <div 
                                    className={this.state.displayBoarding ? "ActiveStyle": "InactiveStyle"}
                                    onClick={this.boardingClicked}>
                                        <p>BOARDING POINT</p>
                                    </div>
                                    <div
                                    className={this.state.displayDropping ? "ActiveStyle": "InactiveStyle"} 
                                    onClick={this.droppingClicked}>
                                        <p>DROPPING POINT</p>
                                    </div>
                                </div>
                                <div className="BDBody">
                                    <div className={this.state.displayBoarding ? "BDWrapper" : ""}>
                                        {
                                            this.state.displayBoarding && 
                                            this.props.boardingPoints.map((point) => (
                                                <div key={point} className="form-check PointStyle">
                                                    <input 
                                                    className="form-check-input" 
                                                    type="radio" 
                                                    name="flexRadioDefault" 
                                                    id={point} 
                                                    value={point}  
                                                    onChange={this.handleBoarding} 
                                                    checked={this.state.selectedBoarding === point ? true : false}/>
                                                    <div className="TimeStyle">
                                                        <p>{this.props.departure}</p>
                                                    </div>
                                                    <label className="form-check-label LabelStyle" htmlFor={point}>
                                                        {point}
                                                    </label>
                                                </div>
                                            ))
                                        }
                                    </div>
                                    <div className={this.state.displayDropping ? "BDWrapper" : ""}>
                                        {
                                            this.state.displayDropping && 
                                            this.props.droppingPoints.map((point) => (
                                                <div key={point} className="form-check PointStyle">
                                                    <input 
                                                    className="form-check-input" 
                                                    type="radio"
                                                    name="flexRadioDefault" 
                                                    id={point} 
                                                    value={point}  
                                                    onChange={this.handleDropping}
                                                    checked={this.state.selectedDropping === point ? true : false} />
                                                    <div className="TimeStyle">
                                                        <p>{this.props.arrival}</p>
                                                    </div>
                                                    <label className="form-check-label LabelStyle" htmlFor={point}>
                                                        {point}
                                                    </label>
                                                </div>
                                            ))
                                        }
                                    </div>
                                    <div className="AmountWrapper">
                                        <p>Amount</p>
                                        <p className="fw-bold">INR {totalAmount}</p>
                                    </div>
                                    {
                                        (this.state.selectedBoarding !== '' && this.state.selectedDropping !== '') &&
                                        <button className="btn BtnStyle" onClick={this.continueClicked}>Continue</button>
                                    }
                                </div>
                            </div>
                        }
                        {
                            !this.state.isUserSelectingBD &&
                            <div className="BDInfoWrapper">
                                <div className="InfoHeading" onClick={this.continueClicked}>
                                    <h5 className="fw-bold">Boarding & Dropping</h5>
                                    <p className="link-primary CursorPointer">CHANGE</p>
                                </div>
                                <div className="LineBDWrapper">
                                    <div className="RouteStyle">
                                        <div className="StartDot"></div>
                                        <div className="DashedLine"></div>
                                        <div className="EndDot"></div>
                                    </div>
                                    <div className="SelectedBDWrapper">
                                        <div className="PlaceStyle">
                                            <p>{this.state.selectedBoarding}</p> 
                                            <p className="fw-bold">{this.props.departure}</p>
                                        </div>
                                        <div className="PlaceStyle">
                                            <p>{this.state.selectedDropping}</p>
                                            <p className="fw-bold">{this.props.arrival}</p>
                                        </div>
                                    </div>
                                </div>
                                {
                                    <div className="SelectedSeats">
                                        <p className="fw-bold">Seat No.</p>
                                        <div className="SeatNameArray">
                                            {
                                                selectedSeats.map((seatName) => (
                                                    <p className="fw-bold" key={seatName}>&nbsp; {seatName}</p> 
                                                ))
                                            }
                                        </div>
                                    </div>
                                }
                                <p className="fw-bold">Fare Details</p>
                                <div className="AmountWrapper">
                                    <p>Amount</p>
                                    <p className="fw-bold">INR {totalAmount}</p>
                                </div>
                                <button className="btn BtnStyle" onClick={this.handleProceedToBook}>PROCEED TO BOOK</button>
                            </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default PaymentDetails

import React, { Component } from 'react';
import { GiSteeringWheel } from 'react-icons/gi';
import './Seat.css';
import SeatDetails from './seatDetails/SeatDetails';

export class Seats extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedSeats: [],
            selectedSeatsObject: []
        }
    }
    userSelectedSeat = (seatName,id) => {
        this.setState({
            selectedSeats: [...this.state.selectedSeats, seatName],
            selectedSeatsObject: [...this.state.selectedSeatsObject, {id,seatName}]
        }, () => {
            this.props.getSelectedSeat(this.state.selectedSeats,this.state.selectedSeatsObject);
        });
    }
    userRemovedSeat = (seatName,id) => {
        let newSelectedSeats = [...this.state.selectedSeats];
        let index = newSelectedSeats.indexOf(seatName);
        newSelectedSeats.splice(index,1);

        let newSelectedSeatsObject = [...this.state.selectedSeatsObject];
        let index2 = newSelectedSeatsObject.map((obj) => obj.id).findIndex((ObjId) => id===ObjId);
        newSelectedSeatsObject.splice(index2,1);
        this.setState({
            selectedSeats: newSelectedSeats,
            selectedSeatsObject: newSelectedSeatsObject
        }, () => {
            this.props.getSelectedSeat(this.state.selectedSeats,this.state.selectedSeatsObject);
        })
    }
    render() {
        return (
            <div className="card CardWrapper">
                <GiSteeringWheel className="SteeringWheel"/>
                <div className="Line"></div>
                <div className="SeatsWrapper">
                    {
                            this.props.seatData.map((seat) => (
                                <SeatDetails 
                                key={seat.id} 
                                seat={seat} 
                                userSelectedSeat={this.userSelectedSeat}
                                userRemovedSeat={this.userRemovedSeat}/>
                            ))
                    }
                </div>
            </div>
        
        )
    }
}

export default Seats

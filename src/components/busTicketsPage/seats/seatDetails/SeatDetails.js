import React, { Component } from 'react';
import './SeatDetails.css';

export class SeatDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelected: false
        }
    }
    handleVacantSeatClick = () => {
        let { id,seatname } = this.props.seat;
        this.setState({
            isSelected: !this.state.isSelected
        }, () => {
            if(this.state.isSelected) {
                this.props.userSelectedSeat(seatname,id);
            } else {
                this.props.userRemovedSeat(seatname,id);
            }
        })
    }
    handleBookedSeatClick = () => {
    }
    render() {
        let seat  = this.props.seat;
        return (
            <div 
            className={seat['is_booked']? "BookedStyle" : (this.state.isSelected ? "SelectedStyle" : "VacantStyle")}
            onClick={!seat['is_booked'] ? this.handleVacantSeatClick : this.handleBookedSeatClick}>
            <p className={this.state.isSelected ? "DisplaySeatName" : "HideSeatName"}>{seat.seatname}</p>
            </div>
        )
    }
}

export default SeatDetails

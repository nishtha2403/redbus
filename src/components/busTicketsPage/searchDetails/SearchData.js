import React, { Component } from 'react';
import { FaArrowRight } from 'react-icons/fa';
import './SearchData.css';


export class SearchData extends Component {
    render() {
        let { from, to, date } = this.props.searchedData;
        return (
            <div>
                <div className="card SearchDataWrapper">
                    <div className="card-body SearchedDataStyle">
                        <p>{from}</p>
                        <div className="ArrowStyle">
                            <FaArrowRight />
                        </div>
                        <p>{to}</p>
                        <p>{date}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default SearchData

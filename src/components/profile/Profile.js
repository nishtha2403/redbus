import React, { Component } from 'react'

export class Profile extends Component {
    render() {
        return (
            <div>
                {
                    localStorage.getItem('username') ? <p>{localStorage.getItem('username')}</p> : <p>Guest</p>
                }
            </div>
        )
    }
}

export default Profile

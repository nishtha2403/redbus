import React, { Component } from 'react';
import SearchBar from '../searchBar/SearchBar';
import Offer from '../Offer/Offer';
import Benefits from '../Benefits/Benefits';
import Footer from '../Footer/Footer';

export class Home extends Component {
    render() {
        return (
            <> 
                <div>
                    <SearchBar handleSearchData={this.props.handleSearchData}/>
                    <Offer />
                    <Benefits />
                </div>
                <Footer />
            </>
        )
    }
}

export default Home;

import React, { Component } from 'react';
import './Footer.css';
import Logo from '../../assets/logo.png';

class Footer extends Component {
  render() {
    return (
      <div className="Footer">
        <ul className="Footer-columns">
          <li className="mb-3">About redBus</li>
          <li>
            <a href="/">About Us </a>
          </li>
          <li>
            <a href="/">Careers</a>
          </li>
          <li>
            <a href="/">Contact Us </a>
          </li>
        </ul>
        <ul className="Footer-columns">
          <li className="mb-3">Info</li>
          <li>
            <a href="/">T &amp; C</a>
          </li>
          <li>
            <a href="/">Privacy Policy</a>
          </li>
          <li>
            <a href="/">FAQ</a>
          </li>
        </ul>
        <ul className="Footer-columns">
          <li className="mb-3">Global Sites</li>
          <li>
            <a href="/">India</a>
          </li>
          <li>
            <a href="/">Singapore</a>
          </li>
          <li>
            <a href="/">Malaysia</a>
          </li>
        </ul>
        <ul className="Footer-columns">
          <li className="mb-3">Our Partners</li>
          <li>
            <a href="/">Goibibo</a>
          </li>
          <li>
            <a href="/">Makemytrip</a>
          </li>
        </ul>
        <div className="Footer-columns">
          <img className="mb-2" src={Logo} alt={Logo} />
          <p>
            redBus is the world's largest online bus ticket booking service
            trusted by over 25 million happy customers globally.
          </p>
          <p>Ⓒ 2021 ibibogroup All rights reserved </p>
        </div>
      </div>
    );
  }
}

export default Footer;

import React, { Component } from 'react';
import './Passenger.css';

class Passenger extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      gender: '',
      age: '',
    };
  }

  handleNameInput = event => {
    this.setState(
      {
        name: event.target.value,
      },
      () => {
        this.props.handleNameChange(this.props, this.state.name);
      }
    );
  };

  handleGenderInput = event => {
    this.setState(
      {
        gender: event.target.value,
      },
      () => {
        this.props.handleGenderChange(this.props, this.state.gender);
      }
    );
  };
  handleAgeInput = event => {
    if (!isNaN(event.target.value)) {
      this.setState(
        {
          age: event.target.value,
        },
        () => {
          this.props.handleAgeChange(this.props, this.state.age);
        }
      );
    }
  };

  render() {
    return (
      <div className="Passenger py-2 mx-4 my-3 pb-4 px-4">
        <p>
          <span className="">Passenger {`${this.props.id + 1}`}</span>
          <span className="px-2">|</span>
          <span className="seat">Seat {this.props.seatno} </span>
        </p>
        <div className="form-group">
          <label
            className="form-label mt-2"
            htmlFor={`Username${this.props.id}`}
          >
            Name
          </label>
          <input
            value={this.state.name}
            onChange={this.handleNameInput}
            placeholder="Name"
            className="form-control"
            type="text"
            id={`Username${this.props.id}`}
            name={`Name${this.props.id}`}
          />
        </div>
        <div className="Row-2">
          <div className="Gender-container">
            <p className="mt-3 mb-1">Gender</p>
            <div className="Gender-option">
              <input
                // value={this.state.gender}
                onChange={this.handleGenderInput}
                className="mx-1"
                type="radio"
                name={`Gender${this.props.id}`}
                id={this.props.id}
                value="male"
              />
              <label className="form-label my-1" htmlFor={this.props.id}>
                Male
              </label>
            </div>
            <div className="Gender-option">
              <input
                onChange={this.handleGenderInput}
                className="mx-1"
                type="radio"
                name={`Gender${this.props.id}`}
                id={this.props.seatno}
                value="female"
              />
              <label className="form-label my-1" htmlFor={this.props.seatno}>
                Female
              </label>
            </div>
          </div>
          <div className="Age-container form-group">
            <label className="form-label mt-3" htmlFor={`Age${this.props.id}`}>
              Age
            </label>
            <input
              value={this.state.age}
              onChange={this.handleAgeInput}
              name={`Age${this.props.id}`}
              placeholder="Age"
              className="form-control"
              type="text"
              id={`Age${this.props.id}`}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Passenger;

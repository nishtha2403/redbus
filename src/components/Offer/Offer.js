import React, { Component } from 'react';
import './Offer.css';
import OfferImg from '../../assets/journey.png';
import RegisterImg from '../../assets/register.svg';

class Offer extends Component {
  render() {
    return (
      <div className="Offer">
        <div className="Register">
          <div>
            <img src={RegisterImg} alt={RegisterImg} />
          </div>
          <div>
            <p>Pre-Register to get timely updates</p>
            <p>Get notified when travel opens up for your route</p>
          </div>
          <div>
            <button className="btn btn-danger py-0">Register Now </button>
          </div>
        </div>
        <div className="Offer-card mt-5">
          <p>Save up for Rs 150 bus tickets </p>
          <img src={OfferImg} alt={OfferImg} />
          <p>Limited Period Offer</p>
          <p>Use code FIRST</p>
        </div>
      </div>
    );
  }
}

export default Offer;

import update from 'immutability-helper';
import React, { Component } from 'react';
import './PassengerDetails.css';
import { FaUserAlt } from 'react-icons/fa';
import Passenger from '../Passenger/Passenger';
import axios from 'axios';

class PassengerDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      passengers: [],
      errorStatus: false,
      isLoading: false,
      errorMessage: 'Something went wrong. Please try after sometime.',
      errorMessageStatus: false,
      successMessage:
        'Thank you for booking with redbus. Your ticket has been successfully sent to your E-mail ID.',
      successMessageStatus: false,
    };
  }

  handleNameChange = (props, name) => {
    if (this.state.passengers[props.id]) {
      const newData = update(this.state, {
        passengers: {
          [props.id]: {
            name: {
              $set: name,
            },
          },
        },
      });
      this.setState(newData, () => {
        console.log(this.state);
      });
    } else {
      let user = {};
      user.name = name;
      const newData = update(this.state, {
        passengers: {
          [props.id]: {
            $set: user,
          },
        },
      });
      this.setState(newData, () => {
        console.log(this.state);
      });
    }
  };

  handleGenderChange = (props, gender) => {
    if (this.state.passengers[props.id]) {
      const newData = update(this.state, {
        passengers: {
          [props.id]: {
            gender: {
              $set: gender,
            },
          },
        },
      });
      this.setState(newData, () => {
        console.log(this.state);
      });
    } else {
      let user = {};
      user.gender = gender;
      const newData = update(this.state, {
        passengers: {
          [props.id]: {
            $set: user,
          },
        },
      });
      this.setState(newData, () => {
        console.log(this.state);
      });
    }
  };

  handleAgeChange = (props, age) => {
    if (this.state.passengers[props.id]) {
      const newData = update(this.state, {
        passengers: {
          [props.id]: {
            age: {
              $set: age,
            },
          },
        },
      });
      this.setState(newData, () => {
        console.log(this.state);
      });
    } else {
      let user = {};
      user.age = age;
      const newData = update(this.state, {
        passengers: {
          [props.id]: {
            $set: user,
          },
        },
      });
      this.setState(newData, () => {
        console.log(this.state);
      });
    }
  };

  validate = numberOfPassengers => {
    let count = 0;
    const arr = this.state.passengers;
    for (let passenger of arr) {
      count += Object.keys(passenger).length;
    }
    if (count !== numberOfPassengers * 3) {
      return false;
    }
    return true;
  };

  handleSubmit = event => {
    event.preventDefault();

    const numberOfPassengers =
      this.props.location.state.seatPaymentInfo.seatInfo.length;

    const isValid = this.validate(numberOfPassengers);

    if (isValid) {
      let passengerData = {
        passengers: this.state.passengers,
      };
      this.setState(
        {
          errorStatus: false,
        },
        () => {
          this.handlePassengerData(passengerData);
        }
      );
    } else {
      this.setState({
        errorStatus: true,
      });
    }
  };

  handlePassengerData = ({ passengers }) => {
    console.log(this.props);
    const {
      amount,
      seatInfo,
      boardPoint: boarding_point,
      droppingPoint: dropping_point,
      busId: busID,
    } = this.props.location.state.seatPaymentInfo;

    for (let i = 0; i < seatInfo.length; i++) {
      passengers[i].seatName = seatInfo[i].seatName;
    }

    const data = {
      busId: busID,
      boardingPoint: boarding_point,
      droppingPoint: dropping_point,
      totalFare: amount,
      passengers: passengers,
    };

    console.log(data);

    const token = localStorage.getItem('accessToken');
    if (token === null) {
      this.props.history.push('/login');
    } else {
      this.setState(
        {
          isLoading: true,
        },
        () => {
          axios
            .post('https://redbus-apis.herokuapp.com/booktickets', data, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then(response => {
              console.log(response);
              this.setState({
                successMessageStatus: true,
                isLoading: false,
              });
            })
            .catch(err => {
              console.log(err);
              this.setState({
                isLoading: false,
                errorMessageStatus: true,
              });
            });
        }
      );
    }
  };

  render() {
    const { amount: totalAmount, seatInfo: passengers } =
      this.props.location.state.seatPaymentInfo;

    const passenger = passengers.map((passenger, index) => {
      const { id, seatName: seatno } = passenger;
      return (
        <Passenger
          handleNameChange={this.handleNameChange}
          handleGenderChange={this.handleGenderChange}
          handleAgeChange={this.handleAgeChange}
          key={id}
          id={index}
          seatno={seatno}
        />
      );
    });
    return (
      <div className="Wrapper">
        <div className="PassengerDetails">
          <div className="PassengerDetails-form">
            <form onSubmit={this.handleSubmit}>
              <h1 className="text-center heading">Passenger Details</h1>
              <h2 className="px-3 py-2">
                <span>
                  <FaUserAlt
                    style={{
                      color: 'white',
                      fontSize: '1.7rem',
                      backgroundColor: '#6ec7b4',
                      borderRadius: '50%',
                      padding: '0.1em ',
                    }}
                  />
                </span>
                <span className="px-2">Passenger information</span>
              </h2>
              {passenger}

              <div className="Fixed-Footer">
                {this.state.errorStatus && (
                  <div className="alert alert-danger text-center" role="alert">
                    Error: Please fill all fields to proceed
                  </div>
                )}
                {this.state.errorMessageStatus && (
                  <div className="alert alert-danger text-center" role="alert">
                    {this.state.errorMessage}
                  </div>
                )}
                {this.state.successMessageStatus && (
                  <div className="alert alert-success" role="alert">
                    {this.state.successMessage}
                  </div>
                )}
                <p>
                  By clicking on proceed, I agree that I have read and
                  understood the TnCs and the Privacy Policy{' '}
                </p>
                <div className="Footer-payment pb-2">
                  <p>
                    Total Amount:
                    <span className="px-4">INR {totalAmount}</span>
                  </p>
                  {this.state.isLoading && (
                    <div
                      className="spinner-border SpinnerStyle"
                      role="status"
                    ></div>
                  )}
                  <button className="btn btn-danger">PROCEED TO PAY</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PassengerDetails;
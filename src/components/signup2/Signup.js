import React, { Component } from 'react';
import './Signup.css';
import SignupImg from '../../assets/signup.svg';
import Logo from '../../assets/logo_red.png';
import axios from 'axios';
import Footer from '../Footer/Footer';

const defaultState = {
  name: '',
  email: '',
  password: '',
  confirmpassword: '',
  nameError: '',
  emailError: '',
  passwordError: '',
  confirmpasswordError: '',
  displayOtpField: false,
  otp: '',
  isOtpVerifying: false
};

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = defaultState;
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  validate = () => {
    let nameError = '';
    let emailError = '';
    let passwordError = '';
    let confirmpasswordError = '';

    //  NAME
    if (this.state.name === '') {
      nameError = 'Name cannot be empty';
    } else if (this.state.name.match(/^[a-z][a-z '-.,]{0,31}$|^$/i) === null) {
      nameError = 'Please choose a valid name';
    }

    if (nameError) {
      this.setState({ nameError });
      return false;
    }

    //  EMAIL
    if (this.state.email === '') {
      emailError = 'Email cannot be empty';
    } else if (
      this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) === null
    ) {
      emailError = 'Please choose a valid email';
    }
    if (emailError) {
      this.setState({ emailError });
      return false;
    }

    // PASSWORD
    if (this.state.password === '') {
      passwordError = 'Password cannot be empty';
    } else if (this.state.password.length < 6) {
      passwordError = 'Password is too short';
    }
    if (passwordError) {
      this.setState({ passwordError });
      return false;
    }

    // CONFIRM PASSWORD
    if (this.state.confirmpassword === '') {
      confirmpasswordError = `Field cannot be empty`;
    } else if (this.state.confirmpassword !== this.state.password) {
      confirmpasswordError = `Password entered doesn't match`;
    }
    if (confirmpasswordError) {
      this.setState({ confirmpasswordError });
      return false;
    }
    return true;
  };

  handleSubmit = event => {
    event.preventDefault();
    const isValid = this.validate();
    if (isValid) {
      let signupData = {
        user_name: this.state.name,
        email: this.state.email,
        password: this.state.password,
      };
      this.handleSignupData(signupData);
      this.setState(defaultState);
    }
  };

  handleSignupData = async signupData => {
    try {
      let response = await axios.post(
        'https://redbus-apis.herokuapp.com/register',
        signupData
      );
      let responseData = await response.data;
      console.log(responseData);
      this.props.history.push('/login');
      this.setState({
        displayOtpField: true
      })
    } catch (err) {
      if (err.response) {
        console.log(err.response.data.msg);
      }
      console.log(err);
    }
  };

  handleOtpChange = (event) => {
    this.setState({
      otp: event.target.value
    })
  }

  verifyOtp = () => {
    this.setState({
      isOtpVerifying: true,
    })
    setTimeout(() => {
      // sending OTP to back-end 
      this.setState({
        isOtpVerifying: false
      })
      this.props.history.push('/');
    }, 500);
    console.log(this.state.otp);
  }

  render() {
    return (
      <>
        {
          !this.state.displayOtpField && 
          <div className="Signup">
            <div className="Signup-img">
              <img src={SignupImg} alt={SignupImg} />
            </div>
            <div className="Signup-content pt-1">
              <img src={Logo} alt={Logo} />
              <p className="mb-4">
                Sign up to avail exciting discounts and cashbacks!!
              </p>
              <form onSubmit={this.handleSubmit}>
                <div className="form-group mb-3">
                  <label className="form-label" htmlFor="name">
                    Name
                  </label>
                  <input
                    className="form-control"
                    type="text"
                    id="name"
                    name="name"
                    placeholder="John Doe"
                    value={this.state.name}
                    onChange={this.handleChange}
                  />
                  <div className="text-danger">{this.state.nameError}</div>
                </div>
                <div className="form-group mb-3">
                  <label className="form-label" htmlFor="email">
                    E-mail
                  </label>
                  <input
                    className="form-control"
                    type="text"
                    id="email"
                    name="email"
                    placeholder="example@domain.com"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                  <div className="text-danger">{this.state.emailError}</div>
                </div>
                <div className="form-group mb-3">
                  <label className="form-label" htmlFor="password">
                    Password
                  </label>
                  <input
                    className="form-control"
                    type="password"
                    id="password"
                    name="password"
                    autoComplete="password"
                    placeholder="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                  <div className="text-danger">{this.state.passwordError}</div>
                </div>
                <div className="form-group mb-3">
                  <label className="form-label" htmlFor="confirmpassword">
                    Confirm Password
                  </label>
                  <input
                    className="form-control"
                    type="password"
                    autoComplete="confirmpassword"
                    id="confirmpassword"
                    name="confirmpassword"
                    placeholder="confirm password"
                    value={this.state.confirmpassword}
                    onChange={this.handleChange}
                  />
                  <div className="text-danger">
                    {this.state.confirmpasswordError}
                  </div>
                </div>
                <button type="submit" className="btn btn-primary">
                  Sign up
                </button>
              </form>
            </div>
        </div>
        }
        {
          this.state.displayOtpField && 
          <div className="OtpFormWrapper">
              <div className="HeadWrapper">
                <h1 className="Heading">Verify OTP</h1>
              </div>
              <div className="OtpForm">
                <label>Enter OTP</label>
                <input className="form-control"
                          type="text" 
                          value={this.state.otp}
                          onChange={this.handleOtpChange}/>
                <div className="BSWrapper">
                      <button className="btn OtpButtonStyle" onClick={this.verifyOtp}>Verify</button>
                      {
                          this.state.isOtpVerifying &&
                          <div className="spinner-border OtpSpinnerStyle" role="status"></div>
                      }
                  </div>
               </div>
          </div>
        }
        <Footer />
      </>
    );
  }
}

export default Signup;

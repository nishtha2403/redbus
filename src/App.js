import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import Home from './components/home2/Home2';
import Login from './components/login/Login';
import Signup from './components/signup2/Signup';
import BusTicketsPage from './components/busTicketsPage/BusTicketsPage';
import PassengerDetails from './components/PassengerDetails/PassengerDetails';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: 'Guest'
    }
  }

  getUsername = (username) => {
    console.log(username);
    this.setState({
      username: username
    })
  }

  render() {
    return (
      <Router>
        <Navbar username={this.state.username}/>
        <div className="App">
          <Route exact path="/" render={props => (
            <Home searchedBus={this.searchedBus}/>
          )}></Route>
          <Route path="/login"  render={props => (
            <Login getUsername={this.getUsername}/>
          )}></Route>
          <Route path="/signup" component={Signup}></Route>
          <Route path="/bus-tickets" render={props => (
            <BusTicketsPage {...props}/>
          )}></Route>
          <Route path="/passenger-details" render={props => (
            <PassengerDetails {...props}/>
          )}></Route>
        </div>
      </Router>
    );
  }
}

export default App;
